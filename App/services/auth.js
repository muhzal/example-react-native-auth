import Axios from 'axios';
import querystring from 'querystring';

export const submitLogin = function({email, password}) {
  return Axios.post(
    'https://keycloak.boost.id/auth/realms/sandbox-react-client/protocol/openid-connect/token',
    querystring.stringify({
      client_id: 'react-app',
      client_secret: '29d47e97-7073-4b00-be84-fdccf00c588e',
      grant_type: 'password',
      username: email,
      password,
    }),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
  );
};
