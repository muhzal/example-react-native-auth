import React, {Fragment, useState, useCallback} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Alert,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {submitLogin} from '../services/auth';

const LoginScreen = props => {
  const [emailState, setEmail] = useState('');
  const [passwordState, setPassword] = useState('');

  const handleSubmit = useCallback(() => {
    console.log(props);
    submitLogin({email: emailState, password: passwordState})
      .then(async response => {
        await AsyncStorage.setItem('userToken', response.data.access_token);
        await AsyncStorage.setItem('refreshToken', response.data.refresh_token);

        Alert.alert('Succes login');

        props.navigation.navigate('App');
      })
      .catch(error => {
        console.log({...error});
        const {data} = error.response;
        if (data && data.error_description) {
          Alert.alert(data.error_description);
        } else {
          Alert.alert('ERROR LOGIN');
        }
      });
  }, [emailState, passwordState, props]);

  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.body}>
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>Login</Text>
          </View>
          <View style={styles.formContainer}>
            <View style={styles.formInput}>
              <Text adjustsFontSizeToFit style={styles.inputText}>
                Email
              </Text>
              <TextInput
                style={styles.input}
                autoCompleteType="email"
                keyboardType="email-address"
                placeholder="abc@example.com"
                onChangeText={text => setEmail(text)}
                value={emailState}
              />
            </View>
            <View style={styles.formInput}>
              <Text style={styles.inputText}>Password</Text>
              <TextInput
                style={styles.input}
                placeholder="Type here to translate!"
                onChangeText={text => setPassword(text)}
                secureTextEntry
                textContentType="password"
                autoCompleteType="password"
                value={passwordState}
              />
            </View>
            <View style={styles.formInput}>
              <TouchableOpacity
                style={styles.loginScreenButton}
                onPress={handleSubmit}
                underlayColor="#fff">
                <Text style={styles.buttonSubmit}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#aaa',
  },
  body: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    height: '100%',
    marginHorizontal: 20,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
    paddingVertical: 12,
    backgroundColor: Colors.primary,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.white,
    textAlign: 'center',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  formContainer: {
    padding: 20,
    backgroundColor: Colors.light,
  },
  formInput: {
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
  },
  input: {
    width: '70%',
    height: 40,
    borderBottomColor: '#000',
    borderBottomWidth: 1,
  },
  inputText: {
    width: '30%',
  },
  buttonSubmit: {
    color: '#fff',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  loginScreenButton: {
    marginTop: 20,
    paddingTop: 10,
    width: '100%',
    paddingBottom: 10,
    backgroundColor: '#1E6738',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
});

export default LoginScreen;
